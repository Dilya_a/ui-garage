$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ


var customInp 	= $(".styler"),
	isotopeBox  = $(".masonry_box"),
  openPopup   = $("[data-open-popup]"),
  fancybox    = $(".fancybox"),
  header      = $("header"),
  subscribe   = $(".subscribe_resp_btn");

if(customInp.length){
  include("js/jquery.formstyler.js");
}

if(isotopeBox.length){
	include("js/isotope.pkgd.js");
	include("js/packery-mode.pkgd.js");
}

if(openPopup.length){
  include("js/jquery.arcticmodal.js");
}
if(fancybox.length){
  // include("js/fancybox/jquery.easing.1.3.js");
  include("js/fancybox/jquery.fancybox.js");
}


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

  if(fancybox.length){
    fancybox.fancybox({
      autoHeight : false,
      autoWidth  : false,
      openEffect  : 'none',
      closeEffect : 'none',
      width : 850, //789
      height : '100%', //800
      "overlayOpacity" : 0 ,
      hideOnContentClick : false,
      centerOnScroll: true,
      autoResize : true,
      autoScale : true,
      autoHeight : false,
      scrolling : false,
      overlay : {
          closeClick : true,  // if true, fancyBox will be closed when user clicks on the overlay
      }

    });
  }


  if(customInp.length){
  	customInp.styler();
  }

  if(isotopeBox.length){
  	$(isotopeBox).isotope({
	  	layoutMode: 'packery',
    	itemSelector: '.masonry_item'
	 })
  }

  openPopup.on("click", function(evnt){
    var popupId = $(this).attr("href");
    $(popupId).arcticmodal();

    event.preventDefault();
  })

  header.on("click", 'button', function(){
    var currentElem = $(this);

    if(currentElem.hasClass('header_filter_btn')){
      $("body").toggleClass("showFilter");
    }

    else if(currentElem.hasClass('header_search_resp_btn')){
      $("body").toggleClass("showSearch");
    }
  })

    $(".subscribe_resp_btn, .sab_close").on('click ontouchstart', function(){
        $("body").toggleClass("showSubscribe");
    })

  $("#overlay").on("click", function(){
    $("body").removeClass('showFilter showSearch')
  })

});

/*   PRELOADER   */

$(window).load(function(){
  

  var preload = $(".hellopreloader");
  

    setTimeout(function(){

      preload.addClass("active");

    },1500);



});
